<?php
include("db.php");
$nombre = '';
$descripcion= '';
$precio = '';
$codigoBarras= '';
$imagen = '';

if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM productos WHERE id=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $descripcion = $row['descripcion'];
    $precio = $row['precio'];
    $codigoBarras = $row['codigo_barras'];
    $imagen = $row['imagen'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $nombre= $_POST['nombre'];
  $descripcion = $_POST['descripcion'];
  $precio= $_POST['precio'];
  $codigoBarras = $_POST['codigo_barras'];
  $imagen= $_POST['imagen'];


  $query = "UPDATE productos SET nombre = '$nombre', descripcion = '$descripcion', precio = '$precio', codigo_barras = '$codigoBarras', imagen = '$imagen' WHERE id=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Producto Actualizado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: index.php');
}

?>
<?php include('includes/header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Actualizar Nombre">
        </div>
        <div class="form-group">
        <textarea name="descripcion" class="form-control" cols="30" rows="10"><?php echo $descripcion;?></textarea>
        </div>
        <div class="form-group">
          <input name="precio" type="number" class="form-control" value="<?php echo $precio; ?>" placeholder="Actualizar Precio">
        </div>
        <div class="form-group">
          <input name="codigo_barras" type="text" class="form-control" value="<?php echo $codigoBarras; ?>" placeholder="Actualizar Código de barras">
        </div>
        <div class="form-group">
          <input name="imagen" type="text" class="form-control" value="<?php echo $imagen; ?>" placeholder="Actualizar Imagen">
        </div>
        <button class="btn-success" name="update">
          Actualizar Producto
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
